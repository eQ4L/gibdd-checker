const TelegramBot = require('node-telegram-bot-api');

const fs = require('fs');
const path = require('path');

const checker = require('../checker/app');

const token = '997132091:AAGlMEmfpYZjimfLDEdi8Nl-fgLmccQ3MZo';

const bot = new TelegramBot(token, { polling: true });

bot.onText(/\/check (.+)/, async (msg, match) => {
  const chatId = msg.chat.id;
  const vin = match[1];

  const info = await checker(vin);

  bot.sendPhoto(chatId, fs.createReadStream(info.screenPath));
});

module.exports = bot;
