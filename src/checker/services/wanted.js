module.exports = async page => {
  try {
    await page.click('a[data-type="wanted"]');

    const r = await page.waitForResponse('https://xn--b1afk4ade.xn--90adear.xn--p1ai/proxy/check/auto/wanted');
    const response = await r.json();

    return response;
  } catch (error) {
    console.log(error);
    return new Error('Возникла ошибка при получении информации о розыске');
  }
};
