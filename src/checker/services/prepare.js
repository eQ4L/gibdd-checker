const puppeteer = require('puppeteer');
const path = require('path');

const waitInstalling = (browser) => {
  return new Promise(resolve => {
    browser.once('targetcreated', async target => {
      if (target.type() === 'page') {
        const page = await target.page();
        await page.waitForResponse('https://getadblock.com/images/AdBlock.svg');
        console.log('Extension installed');
        resolve();
      }
    })
  })
}

module.exports = async () => {
  const extPath = path.resolve(__dirname, '../extensions/adblock');
  const USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/82.0.4072.0 Safari/537.36';

  const browser = await puppeteer.launch({
    args: [
      `--disable-extensions-except=${extPath}`,
      `--load-extension=${extPath}`,
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-infobars',
      '--window-position=0,0',
      '--ignore-certifcate-errors',
      '--ignore-certifcate-errors-spki-list',
      // `--user-agent=${USER_AGENT}`,
      `--window-size=1920, 0`
    ],
    headless: false,
    defaultViewport: null
  });

  await waitInstalling(browser);

  return { browser };
}
