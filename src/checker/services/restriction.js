module.exports = async page => {
  try {
    await page.click('a[data-type="restricted"]');

    const r = await page.waitForResponse('https://xn--b1afk4ade.xn--90adear.xn--p1ai/proxy/check/auto/restrict');
    const response = await r.json();

    return response;
  } catch (error) {
    console.log(error);
    throw new Error('Возникла ошибка при получении информации об ограничениях');
  }
}