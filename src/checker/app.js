const path = require('path');

const prepare = require('./services/prepare');
const checkVin = require('./services/vin');
const checkWanted = require('./services/wanted');
const checkRestriction = require('./services/restriction');

module.exports = async vin => {
  try {
    const { browser } = await prepare();

    const page = await browser.newPage();
    await page.goto('https://xn--90adear.xn--p1ai/check/auto');
    await page.waitForSelector('#checkAutoVIN');
    await page.type('#checkAutoVIN', vin);

    const vinData = await checkVin(page);
    // console.log(vinData.RequestResult.ownershipPeriods.ownershipPeriod);
    const wantedData = await checkWanted(page);
    // console.log(wantedData.RequestResult);
    const restrictionData = await checkRestriction(page);
    // console.log(restrictionData.RequestResult);
    const screenPath = path.resolve(__dirname, './screens', `${vin}.png`);
    await page.screenshot({ path: screenPath, fullPage: true });
    browser.close();

    /**
     * @TODO
     * sending error message
     */
    return {
      vin: vinData.RequestResult.ownershipPeriods.ownershipPeriod,
      wanted: wantedData.RequestResult,
      restriction: restrictionData.RequestResult,
      screenPath
    }
  } catch (error) {
    console.log(error);
  }
}

